package ru.kolesnikov.tm.constant;

public interface TerminalConst {

    String HELP = "help";

    String VERSION = "version";

    String ABOUT = "about";

    String EXIT = "exit";

    String INFO = "info";

    String COMMANDS = "commands";

    String ARGUMENTS = "arguments";

}
