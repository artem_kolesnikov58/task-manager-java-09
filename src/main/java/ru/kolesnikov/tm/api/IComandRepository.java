package ru.kolesnikov.tm.api;

import ru.kolesnikov.tm.model.TerminalCommand;

public interface IComandRepository {

    String[] getCommands(TerminalCommand... values);

    String[] getArgs(TerminalCommand... values);

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
