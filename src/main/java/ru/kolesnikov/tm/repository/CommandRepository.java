package ru.kolesnikov.tm.repository;

import ru.kolesnikov.tm.api.IComandRepository;
import ru.kolesnikov.tm.constant.ArgumentConst;
import ru.kolesnikov.tm.constant.TerminalConst;
import ru.kolesnikov.tm.model.TerminalCommand;

import java.util.Arrays;

public class CommandRepository implements IComandRepository {

    public static final TerminalCommand HELP = new TerminalCommand(
            TerminalConst.HELP, ArgumentConst.HELP, "Display terminal commands."
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Show developer info."
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Show version info."
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            TerminalConst.INFO, ArgumentConst.INFO, "Display information about system."
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            TerminalConst.EXIT, null, "Close application."
    );

    public static final TerminalCommand ARGUMENT = new TerminalCommand(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show program arguments."
    );

    public static final TerminalCommand COMMAND = new TerminalCommand(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Show program commands."
    );

    private static final TerminalCommand[] TERMINAL_COMMANDS = new TerminalCommand[]{
            HELP, ABOUT, VERSION, INFO, COMMAND, ARGUMENT, EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);


    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public TerminalCommand[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }
}
